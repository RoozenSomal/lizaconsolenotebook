﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Person
    {

        private string Name { get; set; }
        private string LastName { get; set; }
        private string FatherName { get; set; }
        private string PhoneNumber { get; set; }
        private string Country { get; set; }
        private int ID;


        public int GetID()
        {
            return ID;
        }
        public string GetName()
        {
            return Name;
        }
        public void SetName(string name)
        {
            Name = name;
        }

        public Person(int id, string Name, string LastName, string FatherName, string PhoneNumber, string Country)
        {
            SetName(Name);
            this.LastName = LastName;
            this.FatherName = FatherName;
            this.PhoneNumber = PhoneNumber;
            this.Country = Country;
            this.ID = id;
        }

        public string ToString()
        {
            return Convert.ToString(ID) + "\t" + Name + " \t" + LastName + " \t" + FatherName + " \t" + PhoneNumber + " \t" + Country + " \t";
        }
        public string ShortString()
        {
            return Name + " \t" + LastName + " \t" + PhoneNumber;
        }

        internal void SetLastName(string s)
        {
            LastName = s;
        }

        internal void SetFatherName(string s)
        {
            FatherName = s;
        }

        internal void SetPhoneNum(string s)
        {
            PhoneNumber = s;
        }

        internal void SetCountry(string s)
        {
            Country = s;
        }
    }

    class ListPerson
    {
        int MaxID = 0;
        List<Person> People;
        public ListPerson(int n)
        {
            People = new List<Person>(n);
            for (int i = 0; i < n; i++)
            {
                Console.WriteLine("Введите ФИО, номер телефона и страну");

                string[] mystring = (Console.ReadLine()).Split(' ');

                Person x = new Person(i, mystring[0], mystring[1], mystring[2], mystring[3], mystring[4]);
                People.Add(x);

                //
            }
        }

        public void Print()
        {
            foreach (Person x in People)
            {
                Console.WriteLine(x.ToString());
            }
        }
        public void ShortPrint()
        {
            foreach (Person x in People)
            {
                Console.WriteLine(x.ShortString());
            }
        }
        public void Print(int i)
        {
            foreach (Person x in People)
            {
                if (x.GetID() == i)
                    Console.WriteLine(x.ToString());
            }
        }
        public void ShortPrint(int i)
        {
            foreach (Person x in People)
            {
                if (x.GetID() == i)
                    Console.WriteLine(x.ShortString());
            }
        }

        public void DeleteByID(int id)
        {
            int del = -1;
            for (int i = 0; i < People.Count; i++)
            {
                if (People[i].GetID() == id)
                    del = i;
            }
            if (del == -1)
                Console.WriteLine("Такой записи не существует.");
            else
                People.RemoveAt(del);
        }

        public void ChangeByID(int id, string s)
        {
            int del = -1;
            for (int i = 0; i < People.Count; i++)
            {
                if (People[i].GetID() == id)
                {
                    del = i;
                    string[] mystring = s.Split(' ');
                    People[i].SetName(mystring[0]);
                    People[i].SetLastName(mystring[1]);
                    People[i].SetFatherName(mystring[2]);
                    People[i].SetPhoneNum(mystring[3]);
                    People[i].SetCountry(mystring[4]);
                }
            }
            if (del == -1)
                Console.WriteLine("Такой записи не существует.");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите количество пользователей");
            int n = Convert.ToInt32(Console.ReadLine());
            ListPerson P = new ListPerson(n);
            Console.WriteLine("0 - сокращенный, 1 - подробный вывод");
            int q = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Список пользователей:");
            if (q != 0)
                P.Print();
            else
                P.ShortPrint();

            Console.WriteLine("Хотите изменить запись?");
            string s = Console.ReadLine();
            if (s == "Yes" || s == "Да" || s == "1" || s == "yes" || s == "да")
            {
                Console.Write("Введите номер изменяемой записи: ");
                int y = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine("Введите ФИО, номер телефона и страну");
                s = Console.ReadLine();
                P.ChangeByID(y, s);
                if (q != 0)
                    P.Print();
                else
                    P.ShortPrint();
            }
            Console.WriteLine("Вы хотите удалить запись?");
            s = Console.ReadLine();
            if (s == "Yes" || s == "Да" || s == "1" || s == "yes" || s == "да")
            {
                Console.WriteLine("Введите номер удаляемого элемента");
                int x = Convert.ToInt32(Console.ReadLine());
                P.DeleteByID(x);
                Console.WriteLine("Список пользователей:");
                if (q != 0)
                    P.Print();
                else
                    P.ShortPrint();
            }

            Console.WriteLine("Конец программы");
            Console.ReadKey();

        }
    }
}
